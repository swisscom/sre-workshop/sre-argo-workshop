# sre-argo-workshop

This repo holds an example deployment for argo and demonstrates argo rollout 

## Setup

This setup is done on Ubuntu

### Install k3s
1. Install https://docs.k3s.io/installation
1. `export KUBECONFIG=/etc/rancher/k3s/k3s.yaml`

### Install argo
1. `kubectl create namespace argocd`
1. `kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml`
1. `kubectl config set-context --current --namespace=argocd`

### Install argo cli
1. `curl -sSL -o argocd-linux-amd64 https://github.com/argoproj/argo-cd/releases/latest/download/argocd-linux-amd64`
1. `sudo install -m 555 argocd-linux-amd64 /usr/local/bin/argocd`
1. `rm argocd-linux-amd64`

### Make argocd accessible
1. `kubectl config set-context --current --namespace=argocd`
1. `kubectl port-forward svc/argocd-server -n argocd 8080:443`

### Access argocd GUI
1. Open Browser on https://localhost:8080 (ignore the uknown certificate)
1. login with user admin
1. Get the admin password: `kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 --decode ; echo`
1. password: <admin pwd from above>

### Access argocd cli
1. `argocd login 127.0.0.1:8080`
1. username: `admin`
1. Get the admin password: `kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 --decode ; echo`
1. password: <admin pwd from above>

### Install Argo Rollouts
1. `kubectl create namespace argo-rollouts`
1. `kubectl apply -n argo-rollouts -f https://github.com/argoproj/argo-rollouts/releases/latest/download/install.yaml`

### Install Argo Rollout kubectl plugin
1. `curl -LO https://github.com/argoproj/argo-rollouts/releases/download/v1.7.0/kubectl-argo-rollouts-linux-arm64`
1. `chmod +x ./kubectl-argo-rollouts-linux-amd64`
1. `sudo mv ./kubectl-argo-rollouts-linux-amd64 /usr/local/bin/kubectl-argo-rollouts`

### Access Argo Rollout Dashboard
1. `kubectl argo rollouts dashboard`
